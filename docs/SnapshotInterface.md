`Stand 12/23`

# SnapshotInterface

```typescript
import {UUID} from "bson";

interface SnapshotInterface {
    _id: UUID.v1
    file_name: string
    path: string
    mimetype: string // "image/png"
    size: number
    gallery: UUID.v1
    filter: string // should be a number
    created_at: Date
}
```