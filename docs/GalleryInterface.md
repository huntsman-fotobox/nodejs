`Stand 12/23`

# GalleryInterface

```typescript
import {UUID} from "bson";

interface GalleryInterface {
    _id: UUID.v1
    name: string
    slug: string
    created_at: Date
}
```