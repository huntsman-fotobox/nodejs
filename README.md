`stand 04/2023`

---

# huntsman api

* nodejs
* nvm

## development

```bash
# database
> docker-compose up database

# server
> nvm use
> npm run dev
```

## production

```bash
> ./start.sh
```