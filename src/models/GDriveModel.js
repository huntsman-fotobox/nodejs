'use strict';

const {google} = require('googleapis');
const path = require('path');
const ImageModel = require('./ImageModel');

/**
 * https://www.youtube.com/watch?v=1y0-IfRW114
 * https://developers.google.com/oauthplayground
 * https://console.cloud.google.com/apis/credentials/oauthclient/723074787310-oe6st1uvbo9n3olliru0gm4eoul4su9j.apps.googleusercontent.com?hl=de&project=benedikt-scherer
 */
class GDriveModel {

    CLIENT_ID = process.env.GDRIVE_CLIENT_ID;
    CLIENT_SECRET = process.env.GDRIVE_CLIENT_SECRET;
    REDIRECT_URI = "https://developers.google.com/oauthplayground";
    REFRESH_TOKEN = process.env.GDRIVE_REFRESH_TOKEN;

    oauth2Client;

    constructor() {
        this.oauth2Client = new google.auth.OAuth2(
            this.CLIENT_ID,
            this.CLIENT_SECRET,
            this.REDIRECT_URI
        )

        this.oauth2Client.setCredentials({refresh_token: this.REFRESH_TOKEN});
    }


    /**
     * upload image to GDrive
     * @param path
     * @param meta
     * @returns {Promise<drive_v3.Schema$File|boolean>}
     */
    async upload(path, meta) {
        const drive = google.drive({
            version: 'v3',
            auth: this.oauth2Client
        });

        try {
            const response = await drive.files.create({
               requestBody: {
                   name: `${meta.path}/${meta.file_name}`,
                   mimeType: meta.mimetype
               },
                media: {
                   mimeType: meta.mimetype,
                    body: `${ImageModel.UPLOAD_DIR}/${path}`
                }
            });

            return response.data;
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = GDriveModel;