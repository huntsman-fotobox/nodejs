'use strict';

const userModel = require('mongodb');
const MongoClient = userModel.MongoClient;

const {Logger} = require('./../utils/Logger');

class MongoModel {

    database = process.env.DB_NAME;
    credentials = (process.env.DB_USER) ? `${process.env.DB_USER}:${process.env.DB_PASSWORD}@` : '';
    url = `mongodb://${this.credentials}${process.env.DB_HOST}`;

    client = new MongoClient(this.url);
    collection;

    constructor(collection) {
        this.collection = collection;
    }


    async ping() {
        try {
            await this.client.connect();
            return await this.client.db(this.database).command({ping: 1})
        } catch (err) {
            Logger.error(`(ping) Something went wrong: ${err}`);
            throw err;
        } finally {
            await this.client.close();
        }
    }


    /**
     *
     * @param query
     * @returns {Promise<*|undefined>}
     */
    async findOne(query) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const options = {};

            return await collection.findOne(query, options);
        } catch (err) {
            Logger.error(`(findOne) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }


    async aggregate(query) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);
            let response = [];

            let cursor = await collection.aggregate(query);
            for await (const doc of cursor) {
                response.push(doc);
            }

            return response;
        } catch (err) {
            Logger.error(`(find) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }


    /**
     *
     * @param doc
     * @returns {Promise<*>}
     */
    async insert(doc) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const result = await collection.insertOne(doc);
            return doc;
        } catch (err) {
            Logger.error(`(insert) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }


    /**
     * update one item
     * @param query
     * @param doc
     */
    async update(query, doc) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const options = {upsert: true};
            const updateDoc = {
                $set: doc,
            };
            const result = await collection.updateOne(query, updateDoc, options);
            return `${result.matchedCount} document(s) matched the filter, updated ${result.modifiedCount} document(s)`;
        } catch (err) {
            Logger.error(`(update) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }

}

module.exports = MongoModel;