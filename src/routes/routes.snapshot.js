'use strict';

const express = require('express');
const router = express.Router();

const SnapshotController = require('./../controller/SnapshotController');

router.get(`/:snapshot`,
    async (req, res) => {
        const {snapshot} = req.params;

        try {
            let result = await SnapshotController.get(snapshot);

            res.status(200).json({
                success: true,
                result
            });

        } catch (e) {
            res.status(500).json({
                success: false,
                result: e
            });
        }
    })

router.post(`/`,
    async (req, res) => {

        try {
            if (!req.files) {
                res.send({
                    success: false,
                    message: 'No file uploaded'
                });
            } else {
                const {snapshot} = req.files;

                let result = await SnapshotController.set(snapshot, req.body);

                res.status(200).json({
                    success: true,
                    result
                });
            }
        } catch (e) {
            res.status(500).json({
                success: false,
                result: e
            });
        }
    });

module.exports = router;