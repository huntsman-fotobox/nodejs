let jwt = require('express-jwt');
let jwks = require('jwks-rsa');

class Auth {

    /**
     * validate JWT for registered user
     * @returns {jwt.RequestHandler | *}
     */
    static isRegistered() {
        return jwt({
            secret: jwks.expressJwtSecret({
                cache: true,
                rateLimit: true,
                jwksRequestsPerMinute: 5,
                jwksUri: `${process.env.AUTH0_ISSUER}/.well-known/jwks.json`
            }),
            audience: process.env.AUTH0_AUDIENCE,
            issuer: process.env.AUTH0_ISSUER,
            algorithms: ['RS256']
        });
    }

    /**+
     * check user permissions
     * e.g. ['admin:read']
     * @param perms
     * @returns {Handler}
     */
    static hasPermission(perms) {
        let guard = require('express-jwt-permissions')({
            requestProperty: 'user',
            permissionsProperty: 'permissions'
        });

        return guard.check(perms);
    }
}

module.exports = Auth;
