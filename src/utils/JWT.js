let atob = require("atob");

class JWT {

    /**
     * get user id from request header
     * @param headers
     * @returns {null}
     */
    static user(headers) {
        const bearer = headers['authorization'];
        let auth = null;

        if (bearer) {
            let token = bearer.split(' ')[1];
            let sub = this.parse(token)['sub'];
            auth = {
                provider: sub.split('|')[0],
                id: sub.split('|')[1]
            };
        }

        return auth;
    }


    /**
     * get user id
     * @param headers
     * @returns {null}
     */
    static id(headers) {
        const auth = this.user(headers);
        let id = null;

        if(auth){
            id = auth.id
        }

        return id;
    }


    /**
     * parse JWT
     * @param token
     * @returns {any}
     */
    static parse(token) {
        let base64Url = token.split('.')[1];
        let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        let jsonPayload = decodeURIComponent(atob(base64).split('').map((c) => {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    };

}

module.exports = JWT;
