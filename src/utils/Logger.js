const winston = require("winston");
require('winston-daily-rotate-file');

let transportLogger = new winston.transports.DailyRotateFile({
    filename: 'logs/all.log',
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    json: false,
    keep: 50,
    size: '100m',
    level: 'debug',
    timestamp: function () {
        return (new Date()).toString();
    }
});

let Logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: function () {
                return (new Date()).toString();
            },
            colorize: true,
            level: 'debug'
        }),
        transportLogger
    ]
});

module.exports = {Logger};